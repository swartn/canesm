#!/bin/bash
# This script can be run in RUNPATH, above the level of individual run directories.
# It will diff the official answers file (at the path specified below), with the final.state
# file in the restarts of all simulations in the current directory ending in "$runid_suffix" (specified below)
# "flag" can also be switched from "prod" to "repro" as needed
runid_suffix=mr480
answers_dir=~/canesm/tests/reference-ensembles/answers
flag="prod"
# Default flag value
flag="prod"

for i in *-$runid_suffix; do 
   echo "Checking ${i}" 
   exp=`basename $i ${runid_suffix}`
   exp="${exp%-}" # strip trailing "-"
   if [ "${exp}" == "omip" ] ; then
        ogcm_final=${answers_dir}/${exp}.${flag}.*.final.state
        cdate=`basename $ogcm_final | cut -d'.' -f3`
        diff $i/data/mc_${i}_${cdate}_tiled_nemors.001/rs_final.state ${answers_dir}/${exp}.${flag}.*.final.state
   else
        agcm_final=${answers_dir}/${exp}.${flag}.*.agcm.final.state
        cdate=`basename $agcm_final | cut -d'.' -f3`
        diff $i/data/mc_${i}_${cdate}_agcmrs.001/agcmrs_agcm.final.state ${answers_dir}/${exp}.${flag}.${cdate}.agcm.final.state
   fi
done