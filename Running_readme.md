# Running CanESM and its components from version control

### NB: These instructions only apply to ECCC's machines. No other machines are supported.

## Introduction

This document describes the five basic steps required to run CanESM or its components from Version Control. If this is
your first run, see the section "One time setup" below. For instructions on how to modify the code see 
[Developing\_readme.md](Developing_readme.md). For more
information on the system and its rationale see [README.md](README.md) and
[twiki](http://wiki.cccma.ec.gc.ca/~acrnncs/CanESM5_development_guide.pdf).

### Runid Guidance
 
 As part creating a run, users must select a "runid", or run identifier, that will be used to differeniate
 their runs from others. In _general_ the chosen runids are free form, but there are _some_ restrictions,
 specifically, they must contain **only lower case alphanumeric characters [a-z] and [0-9], the hyphen "-" and the period "."**

## Setting up a run 

1. **Call ``setup-canesm``**:

    **In the directory you want the "run directory" created and setup**, call `setup-canesm`
    specifying at a minimum the runid and version of the code to use (see `setup-canesm -h` for
    additional information) and the high level configuration:

        setup-canesm  ver=ABC config=ESM runid=XXX
        
    where
     * `ver` is a tag/commit/SHA1 checksum from the `CanESM` "super repo" (see Model versions section below, or you could use the branch name such as ``develop_canesm``)
     * `config` defines the high level config you'd like (can be `ESM`, `OMIP`, or `AMIP`),
     * `runid` is the unique runid (**do not re-use runids of existing runs, even if setting up on a different account**).

    Users should see `setup-canesm -h` for additional interface options, but the most pertinent
    optional argument is `repo`, which can be used to specify a user fork.
    
    **CAUTION:** you can specify a branch as a `ver`. While this is a perfectly normal and valid way to
    operate in development, note that the branch will evolve as people make new commits, so be
    aware of which commit is actually used. If reproducibility is one of your main concerns,
    use a tagged version of an explicit commit hash.

2. **Source the run-time environment**
    
    After running `setup-canesm`, you will see output like:

        Setup complete! Now:

           cd your-runid
           source env_setup_file

        to set the proper environment for this run

    Follow these directions to get the proper run-time environment, which
    will place the proper scripts on your `$PATH`, noting that the environment
    file will need to be re-sourced if you log out.

3. **Set your configuration settings in `canesm.cfg` and run `config-canesm`**

    Edit the file `canesm.cfg`, to set dates and run options, as documented in
    that file, and then generate the downstream config files by executing `config-canesm`, i.e.
    
        vi canesm.cfg
        ...              # Change start and end dates, etc. runid has been set already by setup-canesm

        config-canesm
    
    This will produce the config files **that get used by the run** and store them in a
    local `config` directory.
    
    **Note**: unless you run `config-canesm`, your changes to `canesm.cfg` won't take effect.
    
4. **Compile the executables**

    Executables are compiled interactively by the user. Upon sourcing the
    run's environment file, `compile-canesm.sh` will be placed on your `$PATH`.
    Simply execute it to compile:
    
        compile-canesm.sh
        
    The compilation will take a few minutes. See `compile-canesm.sh -h` for
    additional options, but the default behaviour will be to compile the
    executables in the source repo, and link them back to `$EXEC_STORAGE_DIR`
    (defined in `canesm.cfg`), which by default is a local `executables`
    directory.
    
    **Note**: if you don't see all the expected executables, or `compile-canesm.h`
    throws errors, inspect the hidden `.compile*` logs in the working directory.

4. **Save restarts to the run's file database**
    
    Sourcing the run's environment will also add `save_restart_files` and
    `tapeload_rs` to your `$PATH` - use these scripts to retrieve/setup the input
    restarts (defined in `canesm.cfg`) and save them to the run's local file database:

        save_restart_files
        
    or 
    
        tapeload_rs
        
    Where `save_restart_files` looks for the specified restarts in the databases
    defined by `DATAPATH_DB` in the environment, and `tapeload_rs` looks for them
    on the tape archives.
    
    **Note**: these scripts must be ran on the hall you plan to run the model on
    (defined by `compute_system` in `canesm.cfg`)

5. **Submit the job.**

    To launch the experiment, you have two options:
    
    _Via the command line_
    
        expbegin -e ${SEQ_EXP_HOME} -d YYYYMMDDhh # where YYYYMMDDhh should be replaced by todays date (i.e. 2022062218)
        
    or
    
    _Via `tlclient`_
    
    1. log onto `tlclient`
    2. in a terminal navigate to the run directory and `source env_setup_file`
    3. run `xflow` and:
    
        1. set the experiment date and click the green checkmark beside it
        2. right click on the `/canesm` node and select "Submit"


## Continuing an existing run

### Run has crashed before its scheduled end

If a run crashes, the normal recovery procedure is to use `xflow` on `tlclient` to
figure out what job failed, and attempt a re-submission of the inidividual job node
(right click on the offending node and select the "Submit and Continue" option). If this
doesn't work, users should then select the "Listing" menu and then "Latest Abort Listing"
to see the output from the job, and debug the problem. Once the problem is fixed, simply
resubmit the job and the run should continue.

#### Using a Terminal

It should be noted that it is also possible to determine what job has failed by looking
under `$WRK_DIR/sequencer/sequencing/status` (using `tree` works well for this) and
looking for `*abort.stop` files.

Once the offending job is identified, users should be able to find a corresponding, compressed,
`*abort*` "listing file" (output file) under `$WRK_DIR/sequencer/listings/latest/canesm`.
Given that these files are typically compressed via `gzip`, users can inspect these
via something like

    gunzip -c sequencer/listings/latest/path/to/desired/listing_file | less
    
or using some other editor that can open `gzip` files. Then, after the problem is fixed, the 
offending job can be resubmitted via the `maestro` binary, like this:

    maestro -n /path/to/job/node/in/maestro/suite -s submit -f continue [-l loop_name=ITERATION_NUM ]
    
For example, if a user wishes to resubmit the `model_run` job, for the 2nd iteration of the
`model_loop`, it would look like:

    maestro -n /canesm/model/model_loop/model_run -s submit -f continue -l model_loop=2
    
It should be noted that _while users can monitor/relaunch runs purely in the terminal_, it
requires an advanced understanding of the `maestro` sequencing system and as such the `tlclient`/`xflow`
solution is recommended.

### Run has reached the end

To continue a run which has already finished, users should:

1. if _necessary_ `run_stop_time` to reflect the extended run
2. update `start_time` to reflect the new start time - for example if `stop_time` was `6010` in the previous segment, set the new `start_time` to be `6011`
3. update `stop_time` to reflect the new stop time
4. re-run `config-canesm`
5. submit the new run segment, via:

    ```
    expbegin -e $SEQ_EXP_HOME -d YYYYMMDDHH
    # note that if you use the same experiment date stamp as the previous segment, the temporary directories will be overwritten (which isn't bad unless you want them for reference - i.e. listing files)
    # If you use a new datestamp, it will create a new temporary experiment space
    ```
   
   or after reopening `xflow` (to update the counters displayed), define/choose the desired experiment datestamp and select the `canesm` node and hit submit

## Model versions

The branch `develop_canesm` is used to integrate in new changes, and always reflects the lastest developments. We
strive to keep `develop_canesm` stable and even bit-identical to the previous tagged release, but issues can arise.

When important changes occur, a tagged release is issued. Tagged releases are thoroughly tested, stable versions
of the model (although old taggaed releases might not function as HPC changes). The latest tagged release should
always be functional and stable, and represents a reliable starting point for work.
    
To find the latest tagged release, visit [CanESM5 repository on gitlab](https://gitlab.science.gc.ca/CanESM/CanESM5). 
From the top horizontal menu bar, select 'repository'. Then from the secondary menu bar, select 'Tags'. The latest tagged
release is listed at the top, with its commit number underneath. Use that for 'ver=' in the call to setup-canesm."

When new tagged releases are issued, users should merge these into their working branches ASAP.

## One time setup

### Adding your ssh keys to gitlab

Prior to accessing the necessary gitlab repositories, you must add you ssh keys to
gitlab. Do to this, follow
[these](https://wiki.cmc.ec.gc.ca/wiki/Subscribing_To_Gitlab) instructions to
add your keys.

### General Environment Guidance

To run the CanESM system, users must have a few things setup in the `.profile`
along with a `.condarc` file to pick up the necessary infrastructure environments.

Specifically, users must have the following in their `~/.profile`:

    export CCCMA_REF=/home/scrd102/cccma_libs/cccma/latest/                  # defines the lib version to use
    export PATH=$CCCMA_REF/CanESM_source_link/CCCma_tools/tools:$PATH        # Access to setup/s scripts
    source $CCCMA_REF/CanESM_source_link/CCCma_tools/generic/u2_site_profile # basic ordenv setup & ssm loads of maestro etc
    alias load_cccma_env='source $CCCMA_REF/env_setup_file'                  # command to activate a full env with binaries like ggstat
    umask 022
    
and the following in a `~/.condarc` file:

    envs_dirs:
      - /home/scrd102/cccma_conda/envs

Once added, log back in for these changes to take affect.

**Warning**: the environment system used on the ECCC system has **notable** issues if
a `~/.bashrc` file. Make sure this is not used on your science network account.

### One Time Maestro Setup

Prior to using the `maestro` sequencing system, users must setup some maestro files/links
and initialize their `maestro`. Assuming users have setup their `.profile` as laid out
above, to do this:

1. Set the default machine that all maestro suites will use (many specific suites will have machines explicitly defined which will override this):

    ```bash
    mkdir -p ~/.suites
    echo "SEQ_DEFAULT_MACHINE=ppp6" >> ~/.suites/default_resources.def
    ```
    
2. Set the default links, which `maestro` uses to find the locations to place temporary directories

    ```bash
    mkdir -p ~/.suites/.default_links
    ss_scratch_space=/eccc/crd/ccrn/ccrn_tmp/$(whoami)/maestro
    mkdir -p /space/hall6/sitestore/${ss_scratch_space}
    mkdir -p /space/hall5/sitestore/${ss_scratch_space}
    ln -s /space/hall6/sitestore/${ss_scratch_space} ~/.suites/.default_links/ppp6
    ln -s /space/hall6/sitestore/${ss_scratch_space} ~/.suites/.default_links/robert
    ln -s /space/hall5/sitestore/${ss_scratch_space} ~/.suites/.default_links/ppp5
    ln -s /space/hall5/sitestore/${ss_scratch_space} ~/.suites/.default_links/underhill
    ```
    
3. Initialize your `maestro` server, which monitors your "suites" by executing

    ```
    mserver_initSCI
    ```
    
    and following the on-screen prompts, **noting that**:
    
      - for `Enter a machine where you will run your maestro server [default maestro5]:` **enter** **`maestro5-u2`**
      - for `Host to use for hcron. Use full name, such as example. [default hcron-dev1.science.gc.ca]`: **enter** **`hcron-dev5.science.gc.ca`** to target U2 `hcron` servers
      - if you complaints about a "Preference file not found" in reference to a `.maestrorc` file,  don't worry about it - these are inconsequential
    
    Then execute
   
    ```
    mserver
    ```